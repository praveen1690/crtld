package tests;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CreateLead {

	@Test
	public void Signin() throws InterruptedException 
	{
	//adding chrome properties	
	System.setProperty("webdriver.chrome.driver","C:\\\\Users\\\\PRAVEEN\\\\Documents\\\\ChromeDriverLatest\\\\chromedriver.exe");
    //opening chrome browser
	WebDriver driver = new ChromeDriver();
	//opening login page
    driver.get("http://leaftaps.com/opentaps");
    //maximizing the window
    driver.manage().window().maximize();
    //username
    driver.findElement(By.id("username")).sendKeys("DemoSalesManager");
    //password
    driver.findElement(By.id("password")).sendKeys("crmsfa");
    //login button
    driver.findElement(By.className("decorativeSubmit")).click();
    Thread.sleep(2500);
    //click on CRMSFA link
    driver.findElement(By.linkText("CRM/SFA")).click();
    //click on leads button 
    driver.findElement(By.linkText("Leads")).click();
  //click on find leads button  
    driver.findElement(By.linkText("Find Leads")).click();
  //sending keys in first name
    driver.findElement(By.xpath("//div[@id= 'findLeads']//input[@name = 'firstName']")).sendKeys("sarath");
  //driver.findElement(By.xpath("//input[@name='firstname'])[3]")).sendKeys("Sarath");
    driver.findElement(By.xpath("(//button[@class='x-btn-text'])[7]")).click();
  //driver.findElement(By.linkText("10362")).click();
    Thread.sleep(3000);

    List<WebElement> we= driver.findElements(By.xpath("//div[contains(@class, 'x-panel-body')]//a[@class= 'linktext']"));
   System.out.println("web element size is " + we.size());
    we.get(0).click();
    
    driver.findElement(By.xpath("(//a[@class='subMenuButton'])[3]")).click();
    driver.findElement(By.xpath("(//input[@name='firstName'])[3]")).clear();
    driver.findElement(By.xpath("(//input[@name='firstName'])[3]")).sendKeys("prvn");
    driver.findElement(By.xpath("(//input[@name='submitButton'])[1]")).click();
    Thread.sleep(2000);
    String text = driver.findElement(By.xpath("(//span[@class='tabletext'])[4]")).getText();
    System.out.println(text);
    driver.quit();
    
	}

}
