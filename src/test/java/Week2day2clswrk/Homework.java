package Week2day2clswrk;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Homework {

	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver","C:\\\\Users\\\\PRAVEEN\\\\Documents\\\\ChromeDriverLatest\\\\chromedriver.exe");
	    //opening chrome browser
		WebDriver driver = new ChromeDriver();
		//opening login page
	    driver.get("http://leaftaps.com/opentaps");
	    //maximizing the window
	    driver.manage().window().maximize();
	    //username
	    driver.findElement(By.id("username")).sendKeys("DemoSalesManager");
	    //password
	    driver.findElement(By.id("password")).sendKeys("crmsfa");
	    //login button
	    driver.findElement(By.className("decorativeSubmit")).click();
	    Thread.sleep(2500);
	    //click on CRMSFA link
	    driver.findElement(By.linkText("CRM/SFA")).click();
	    //click on leads button 
	    driver.findElement(By.linkText("Leads")).click();
	    driver.findElement(By.linkText("Merge Leads")).click();
	    driver.findElement(By.xpath("(//table[@class='twoColumnForm']//img)[2]")).click();
	    Set<String> allwindows=driver.getWindowHandles();
	    List<String> list=new ArrayList<String>();
	    list.addAll(allwindows);
	    System.out.println(driver.getTitle());
	    driver.switchTo().window(list.get(1));
	    System.out.println(driver.getCurrentUrl());
	    System.out.println(driver.getTitle());
	    driver.findElement(By.xpath("")).sendKeys("");


	}

}
